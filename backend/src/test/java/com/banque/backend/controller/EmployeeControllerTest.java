package com.banque.backend.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.banque.backend.entity.Client;
import com.banque.backend.entity.Niveau;
import com.banque.backend.service.EmployeeService;
import com.banque.contrat.api.serveur.employee.model.ClientDto;

@ExtendWith(MockitoExtension.class)
class EmployeeControllerTest {

	@Mock
	private EmployeeService mockClientService;

	@InjectMocks
	private EmployeeController classToTest;

//	@Test
//	void testGetProduitsClient() {
//		MockHttpServletRequest request = new MockHttpServletRequest();
//		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
//
//		List<Produit> produits = new ArrayList<Produit>();
//		Produit produit = new Produit("produitId", "nom", Approbation.BOTH, new Niveau("niveauId", "description", 1));
//		produits.add(produit);
//		Mockito.when(mockClientService.listProduitByClientNomEquals(Mockito.anyString())).thenReturn(produits);
//
//		ResponseEntity<List<ProduitDto>> reponse = classToTest.getProduitsClient("bob", false);
//
//		Assertions.assertEquals(200, reponse.getStatusCodeValue());
//		Assertions.assertFalse(reponse.getBody().isEmpty());
//
//		Iterator<ProduitDto> itr = reponse.getBody().iterator();
//		while (itr.hasNext()) {
//			ProduitDto reponseProduitDto = itr.next();
//			Assertions.assertEquals(produit.getProduitId(), reponseProduitDto.getProduitId());
//			Assertions.assertEquals(produit.getNom(), reponseProduitDto.getNom());
//
//		}
//
//	}

	@Test
	void ajoutClient() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		Client client = new Client("Bob", new Niveau("N1", "Une description", 1));
		Mockito.when(mockClientService.addClient(Mockito.anyString())).thenReturn(client);

		String employeeToAdd = "Bob";
		ResponseEntity<ClientDto> reponse = classToTest.ajoutClient(employeeToAdd);

		Assertions.assertEquals(201, reponse.getStatusCodeValue());
		Assertions.assertEquals(employeeToAdd, reponse.getBody().getNom());

	}

}
