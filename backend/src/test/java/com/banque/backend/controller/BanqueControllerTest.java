package com.banque.backend.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@ExtendWith(MockitoExtension.class)
class BanqueControllerTest {

	@InjectMocks
	private BanqueController classToTest;

	@Test
	void testGetStatus() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		ResponseEntity<String> reponse = classToTest.getStatus();

		Assertions.assertEquals(200, reponse.getStatusCodeValue());
		Assertions.assertEquals("I'm alive", reponse.getBody());

	}

}
