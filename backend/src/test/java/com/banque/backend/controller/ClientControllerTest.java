package com.banque.backend.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Niveau;
import com.banque.backend.entity.Produit;
import com.banque.backend.service.ClientService;
import com.banque.backend.utilitaire.Approbation;
import com.banque.backend.utilitaire.Statut;
import com.banque.contrat.api.serveur.client.model.ClientProduitDto;

@ExtendWith(MockitoExtension.class)
class ClientControllerTest {

	@Mock
	private ClientService mockClientService;

	@InjectMocks
	private ClientController classToTest;

	private Produit produit;
	private Client client;

	@BeforeEach
	public void initEach() {
		produit = new Produit("produitId", "nom", Approbation.NONE, new Niveau("niveauId", "description", 1));
		client = new Client("UnClient", new Niveau("niveauId", "description", 1));
	}

	@Test
	void getClientProduits() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		List<Produit> produits = new ArrayList<Produit>();
		produits.add(produit);
		Mockito.when(mockClientService.listProduitByClientNomEquals(Mockito.anyString())).thenReturn(produits);

		ResponseEntity<List<ClientProduitDto>> reponse = classToTest.getClientProduits("bob", true);

		Assertions.assertEquals(200, reponse.getStatusCodeValue());
		Assertions.assertFalse(reponse.getBody().isEmpty());

		Iterator<ClientProduitDto> itr = reponse.getBody().iterator();
		while (itr.hasNext()) {
			ClientProduitDto reponseProduitDto = itr.next();
			Assertions.assertEquals(produit.getProduitId(), reponseProduitDto.getProduitId());
			Assertions.assertEquals(produit.getNom(), reponseProduitDto.getNom());

		}

	}

	@Test
	void subscribeProduits() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		ClientProduit clientProduit = new ClientProduit();
		clientProduit.setProduit(produit);
		clientProduit.setClient(client);
		clientProduit.setStatut(Statut.APPROUVE);

		Mockito.when(mockClientService.abonnerProduit(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(clientProduit);

		ResponseEntity<ClientProduitDto> reponse = classToTest.subscribeProduits("bob", "ProduitId");

		Assertions.assertEquals(200, reponse.getStatusCodeValue());
		Assertions.assertNotNull(reponse.getBody());

		Assertions.assertEquals(produit.getProduitId(), reponse.getBody().getProduitId());
		Assertions.assertEquals(produit.getNom(), reponse.getBody().getNom());
		Assertions.assertEquals(Statut.APPROUVE.name(), reponse.getBody().getStatut());
	}

	@Test
	void unsubscribeProduits() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		ClientProduit clientProduit = new ClientProduit();
		clientProduit.setProduit(produit);
		clientProduit.setClient(client);
		clientProduit.setStatut(Statut.EN_ATTENTE_SUPRESSION);

		Mockito.when(mockClientService.desabonnerProduit(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(clientProduit);

		ResponseEntity<ClientProduitDto> reponse = classToTest.unsubscribeProduits("bob", "ProduitId");

		Assertions.assertEquals(200, reponse.getStatusCodeValue());
		Assertions.assertNotNull(reponse.getBody());

		Assertions.assertEquals(produit.getProduitId(), reponse.getBody().getProduitId());
		Assertions.assertEquals(produit.getNom(), reponse.getBody().getNom());
		Assertions.assertEquals(Statut.EN_ATTENTE_SUPRESSION.name(), reponse.getBody().getStatut());
	}

}
