package com.banque.backend.entity;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.banque.backend.util.AssertAnnotations;
import com.banque.backend.util.ReflectTool;

class ClientProduitTest {

	@Test
	public void typeAnnotations() {
		AssertAnnotations.assertType(ClientProduit.class, Entity.class, Table.class);
	}

	@Test
	public void fieldAnnotations() {
		AssertAnnotations.assertField(ClientProduit.class, "id", Id.class, GeneratedValue.class);
		AssertAnnotations.assertField(ClientProduit.class, "client", NotNull.class, ManyToOne.class, JoinColumn.class);
		AssertAnnotations.assertField(ClientProduit.class, "produit", NotNull.class, ManyToOne.class, JoinColumn.class);
		AssertAnnotations.assertField(ClientProduit.class, "statut", Enumerated.class);
	}

	@Test
	public void entity() {
		Entity a = ReflectTool.getClassAnnotation(ClientProduit.class, Entity.class);
		Assertions.assertThat(a.name()).isNotNull();
	}

	@Test
	public void table() {
		Table t = ReflectTool.getClassAnnotation(ClientProduit.class, Table.class);
		Assertions.assertThat(t.name()).isEqualTo("tbl_client_produit");
	}

}
