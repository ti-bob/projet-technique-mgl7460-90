package com.banque.backend.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.banque.backend.util.AssertAnnotations;
import com.banque.backend.util.ReflectTool;

class NiveauTest {

	@Test
	public void typeAnnotations() {
		AssertAnnotations.assertType(Niveau.class, Entity.class, Table.class);
	}

	@Test
	public void fieldAnnotations() {
		AssertAnnotations.assertField(Niveau.class, "id", Id.class, GeneratedValue.class);
		AssertAnnotations.assertField(Niveau.class, "niveauId", NotNull.class);
		AssertAnnotations.assertField(Niveau.class, "description", NotNull.class);
		AssertAnnotations.assertField(Niveau.class, "ordre", NotNull.class);
	}

	@Test
	public void entity() {
		Entity a = ReflectTool.getClassAnnotation(Niveau.class, Entity.class);
		Assertions.assertThat(a.name()).isNotNull();
	}

	@Test
	public void table() {
		Table t = ReflectTool.getClassAnnotation(Niveau.class, Table.class);
		Assertions.assertThat(t.name()).isEqualTo("tbl_niveau");
	}

}
