package com.banque.backend.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.banque.backend.util.AssertAnnotations;
import com.banque.backend.util.ReflectTool;

class ClientTest {

	@Test
	public void typeAnnotations() {
		AssertAnnotations.assertType(Client.class, Entity.class, Table.class);
	}

	@Test
	public void fieldAnnotations() {
		AssertAnnotations.assertField(Client.class, "id", Id.class, GeneratedValue.class);
		AssertAnnotations.assertField(Client.class, "nom", NotNull.class);
		AssertAnnotations.assertField(Client.class, "produits", OneToMany.class);
		AssertAnnotations.assertField(Client.class, "niveau", NotNull.class, OneToOne.class, JoinColumn.class);
	}

	@Test
	public void entity() {
		Entity a = ReflectTool.getClassAnnotation(Client.class, Entity.class);
		Assertions.assertThat(a.name()).isNotNull();
	}

	@Test
	public void table() {
		Table t = ReflectTool.getClassAnnotation(Client.class, Table.class);
		Assertions.assertThat(t.name()).isEqualTo("tbl_client");
	}

}
