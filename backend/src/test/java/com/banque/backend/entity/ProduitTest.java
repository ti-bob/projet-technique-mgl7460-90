package com.banque.backend.entity;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.banque.backend.util.AssertAnnotations;
import com.banque.backend.util.ReflectTool;

class ProduitTest {

	@Test
	public void typeAnnotations() {
		AssertAnnotations.assertType(Produit.class, Entity.class, Table.class);
	}

	@Test
	public void fieldAnnotations() {
		AssertAnnotations.assertField(Produit.class, "id", Id.class, GeneratedValue.class);
		AssertAnnotations.assertField(Produit.class, "produitId", NotNull.class);
		AssertAnnotations.assertField(Produit.class, "nom", NotNull.class);
		AssertAnnotations.assertField(Produit.class, "approbationRequise", NotNull.class, Enumerated.class);
		AssertAnnotations.assertField(Produit.class, "niveau", NotNull.class, OneToOne.class, JoinColumn.class);
		AssertAnnotations.assertField(Produit.class, "clients", OneToMany.class);

	}

	@Test
	public void entity() {
		Entity a = ReflectTool.getClassAnnotation(Produit.class, Entity.class);
		Assertions.assertThat(a.name()).isNotNull();
	}

	@Test
	public void table() {
		Table t = ReflectTool.getClassAnnotation(Produit.class, Table.class);
		Assertions.assertThat(t.name()).isEqualTo("tbl_produit");
	}
}
