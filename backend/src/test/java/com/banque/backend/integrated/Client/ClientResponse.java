package com.banque.backend.integrated.Client;

import static java.util.stream.Collectors.toMap;

import java.util.Map;

import net.serenitybdd.rest.SerenityRest;

public class ClientResponse {
	public Map<String, String> returned() {
		Map<String, Object> body = SerenityRest.lastResponse().getBody().as(Map.class);
		// Object test = SerenityRest.lastResponse().getBody().as(String.class);
		Map<String, String> retval = mapOfStringsFrom(body);
		return retval;
	}

	private Map<String, String> mapOfStringsFrom(Map<String, Object> map) {
		return map.entrySet().stream().collect(toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
	}

}
