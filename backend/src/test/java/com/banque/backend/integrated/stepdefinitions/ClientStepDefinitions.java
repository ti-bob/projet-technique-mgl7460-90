package com.banque.backend.integrated.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.banque.backend.integrated.Client.AddNewClient;
import com.banque.backend.integrated.Client.ClientResponse;
import com.banque.backend.integrated.templates.FieldValues;
import com.banque.backend.integrated.templates.MergeFrom;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ClientStepDefinitions {

	@Steps
	AddNewClient addNewClient;

	@Steps
	ClientResponse theClientDetails;

	String client;

	@Given("the following client:")
	public void the_following_client(io.cucumber.datatable.DataTable dataTable) throws cucumber.api.PendingException {
		List<Map<String, String>> mylist = dataTable.asMaps();

		try {
			client = MergeFrom.template("templates/Client.json")
					.withDefaultValuesFrom(FieldValues.in("templates/newClient.properties"))
					.withFieldsFrom(mylist.get(0));
		} catch (IOException e) {
			throw new cucumber.api.PendingException();

		}
	}

	@When("I add the client")
	public void I_add_the_client() {
		addNewClient.withDetails(client);
	}

	@Then("the recorded client should include the following details:")
	public void the_added_client_should_include_the_following_details(List<Map<String, String>> clientetails) {
		restAssuredThat(response -> response.statusCode(201));

		Map<String, String> expectedResponse = clientetails.get(0);
		Map<String, String> actualResponse = theClientDetails.returned();

		assertThat(actualResponse).containsAllEntriesOf(expectedResponse);
	}

	@Then("the added client should include the following details:")
	public void the_added_client_should_include_the_following_details(io.cucumber.datatable.DataTable dataTable)
			throws cucumber.api.PendingException {
		List<Map<String, String>> clientetails = dataTable.asMaps();

		Map<String, String> expectedResponse = clientetails.get(0);
		Map<String, String> actualResponse = theClientDetails.returned();

		assertThat(actualResponse).containsAllEntriesOf(expectedResponse);
	}

}
