package com.banque.backend.integrated.Produit;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class DemandeClientProduits {

	@Step("demande client produit")
	public void withDetails(String URL) {
		SerenityRest.get(URL);
	}
}
