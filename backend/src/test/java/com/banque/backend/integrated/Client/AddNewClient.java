package com.banque.backend.integrated.Client;

import com.banque.backend.integrated.WebServiceEndPoints;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class AddNewClient {

	@Step("Add a new client")
	public void withDetails(String client) {
		SerenityRest.given().contentType("application/json").header("Content-Type", "application/json").body(client)
				.when().post(WebServiceEndPoints.CLIENT.getUrl());
	}
}
