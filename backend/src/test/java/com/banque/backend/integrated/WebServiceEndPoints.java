package com.banque.backend.integrated;

public enum WebServiceEndPoints {
	STATUS("http://localhost:8080/banque/status"), PRODUIT("http://localhost:8080/clients/"),
	CLIENT("http://localhost:8080/employee/clients");

	private final String url;

	WebServiceEndPoints(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
}
