package com.banque.backend.integrated.stepdefinitions;

import static com.banque.backend.integrated.WebServiceEndPoints.PRODUIT;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.banque.backend.integrated.Client.ClientResponse;
import com.banque.backend.integrated.Produit.DemandeClientProduits;
import com.banque.backend.integrated.templates.FieldValues;
import com.banque.backend.integrated.templates.MergeFrom;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

public class ProduitStepDefinitions {
	String clientProduitParams;

	@Steps
	DemandeClientProduits demandeClientProduits;

	@Steps
	ClientResponse theClientDetails;

	@Given("the following client for product listing:")
	public void the_following_client_for_product_listing(io.cucumber.datatable.DataTable dataTable)
			throws cucumber.api.PendingException {
		List<Map<String, String>> mylist = dataTable.asMaps();
		try {
			clientProduitParams = MergeFrom.template("templates/ClientProduit.json")
					.withDefaultValuesFrom(FieldValues.in("templates/newClient.properties"))
					.withFieldsFrom(mylist.get(0));
		} catch (IOException e) {
			throw new cucumber.api.PendingException();

		}

	}

	@When("I ask for client products")
	public void i_ask_for_client_products() throws cucumber.api.PendingException {

		String finalURL = PRODUIT.getUrl() + clientProduitParams;
		demandeClientProduits.withDetails(finalURL);
	}

	@Then("the client products should include the following details:")
	public void the_client_products_should_include_the_following_details(io.cucumber.datatable.DataTable dataTable) {
		List<Map<String, String>> expectedResponse = dataTable.asMaps();
		List<Map<String, String>> actualResponse = (SerenityRest.lastResponse().getBody().as(ArrayList.class));

		for (int i = 0; i < actualResponse.size(); i++) {

			Map<String, String> o = actualResponse.get(i);
			assertThat(actualResponse.get(i)).containsAllEntriesOf(expectedResponse.get(i));

		}

	}

}
