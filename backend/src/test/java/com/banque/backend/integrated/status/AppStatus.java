package com.banque.backend.integrated.status;

public enum AppStatus {
	RUNNING, DOWN
}
