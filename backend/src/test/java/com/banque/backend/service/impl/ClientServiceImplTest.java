package com.banque.backend.service.impl;

import java.util.HashSet;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Niveau;
import com.banque.backend.entity.Produit;
import com.banque.backend.repository.ClientProduitRepository;
import com.banque.backend.repository.ClientRepository;
import com.banque.backend.repository.NiveauRepository;
import com.banque.backend.repository.ProduitRepository;
import com.banque.backend.utilitaire.Approbation;

@ExtendWith(MockitoExtension.class)
class ClientServiceImplTest {

	@Mock
	private ClientRepository clientRepository;
	@Mock
	private NiveauRepository niveauRepository;
	@Mock
	private ProduitRepository produitRepository;
	@Mock
	private ClientProduitRepository clientProduitRepository;

	@InjectMocks
	private ClientServiceImpl clientServiceImpl;

	private Produit produit;
	private Client client;

	@BeforeEach
	public void initEach() {
		produit = new Produit("produitId", "nom", Approbation.BOTH, new Niveau("niveauId", "description", 1));
		produit.setClients(new HashSet<ClientProduit>());

		client = new Client("UnClient", new Niveau("niveauId", "description", 1));
		client.setProduits(new HashSet<ClientProduit>());
	}

	@Test
	void abonnerProduit() {
		Mockito.when(clientRepository.findByNomEquals(Mockito.anyString())).thenReturn(Optional.of(client));
		Mockito.when(produitRepository.findByProduitIdEquals(Mockito.anyString())).thenReturn(Optional.of(produit));

		ClientProduit clientProduit = new ClientProduit(client, produit);
		Mockito.when(clientProduitRepository.saveAndFlush(Mockito.any(ClientProduit.class))).thenReturn(clientProduit);

		ClientProduit reponse = clientServiceImpl.abonnerProduit("clientNom", "produitID");
		Assertions.assertNotNull(reponse);
	}

}
