package com.banque.backend.service.impl;

import java.util.HashSet;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.banque.backend.controller.exception.NotFoundException;
import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Niveau;
import com.banque.backend.entity.Produit;
import com.banque.backend.repository.ClientProduitRepository;
import com.banque.backend.repository.ClientRepository;
import com.banque.backend.repository.NiveauRepository;
import com.banque.backend.repository.ProduitRepository;
import com.banque.backend.utilitaire.Approbation;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

	@Mock
	private ClientRepository clientRepository;
	@Mock
	private NiveauRepository niveauRepository;
	@Mock
	private ProduitRepository produitRepository;
	@Mock
	private ClientProduitRepository clientProduitRepository;

	@InjectMocks
	private EmployeeServiceImpl classToTest;

	private Produit produit;
	private Client client;

	@BeforeEach
	public void initEach() {
		produit = new Produit("produitId", "nom", Approbation.BOTH, new Niveau("niveauId", "description", 1));
		produit.setClients(new HashSet<ClientProduit>());

		client = new Client("UnClient", new Niveau("niveauId", "description", 1));
		client.setProduits(new HashSet<ClientProduit>());
	}

	@Test
	void addClient() {
		Niveau niveau = new Niveau("N1", "Un niveau", 1);
		Mockito.when(niveauRepository.findByNiveauIdEquals(Mockito.anyString())).thenReturn(Optional.of(niveau));
		Client client = new Client("Bob", niveau);
		Mockito.when(clientRepository.saveAndFlush(Mockito.any(Client.class))).thenReturn(client);

		Client reponse = classToTest.addClient("Bob");
		Assertions.assertNotNull(reponse);
		Mockito.verify(clientRepository).saveAndFlush(ArgumentMatchers.eq(reponse));
	}

	@Test
	void addClient_when_niveauNonTrouve() {
		Mockito.when(niveauRepository.findByNiveauIdEquals(Mockito.anyString())).thenReturn(Optional.empty());

		Exception exception = Assertions.assertThrows(NotFoundException.class, () -> {
			classToTest.addClient("Bob");
		});
		Assertions.assertTrue(exception.getMessage().contains("non trouvé"));
	}

}
