Feature: Add a new client

  Scenario: Each client has a unique ID
    Given the following client:
    | clientNom |
    | Gerry     |
    When I add the client
    Then the added client should include the following details:
    | nom | id |
    | Gerry     | 1     |

    Given the following client:
    | clientNom |
    | David     |
    When I add the client
    Then the added client should include the following details:
    | nom | id |
    | David     | 2     |