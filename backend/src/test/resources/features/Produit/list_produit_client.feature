Feature: list produit client

  Scenario: Each client has 2 products
    Given the following client for product listing:
    | clientNom | souscrit    |
    | Gerry     | false       |
    When I ask for client products
    Then the client products should include the following details:
      | produitId  | nom          |
      | PE1        | Epargne 0,1% |
      | PF1        | Financement 10,0% |

    Given the following client:
      | clientNom | souscrit    |
      | David     | false       |
    When I ask for client products
    Then the client products should include the following details:
      | produitId  | nom          |
      | PE1        | Epargne 0,1% |
      | PF1        | Financement 10,0% |
