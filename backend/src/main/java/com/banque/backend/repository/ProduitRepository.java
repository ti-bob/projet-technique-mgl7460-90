package com.banque.backend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.banque.backend.entity.Produit;

@Repository("produitRepository")
public interface ProduitRepository extends JpaRepository<Produit, Long> {

	@Query("SELECT p FROM Produit p, Niveau n WHERE n = p.niveau and n.ordre <= ?1")
	public List<Produit> findProduitNiveauQuery(int ordre);

	public Optional<Produit> findByProduitIdEquals(String produitId);

}
