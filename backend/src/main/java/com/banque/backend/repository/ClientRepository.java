package com.banque.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banque.backend.entity.Client;

@Repository("clientRepository")
public interface ClientRepository extends JpaRepository<Client, Long> {

	public Optional<Client> findByNomEquals(String nom);

}
