package com.banque.backend.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Produit;
import com.banque.backend.utilitaire.Statut;

@Repository("clientProduitRepository")
public interface ClientProduitRepository extends JpaRepository<ClientProduit, Long> {

	public Optional<ClientProduit> findByClientAndProduitEquals(Client client, Produit produit);

	public List<ClientProduit> findAllByStatutInOrderByClientAscProduitNiveauAsc(Set<Statut> statuts);

	public ClientProduit findByClientNomAndProduitProduitId(String clientNom, String produitId);

}
