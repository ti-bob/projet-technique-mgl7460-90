package com.banque.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.banque.backend.entity.Niveau;

@Repository("niveauRepository")
public interface NiveauRepository extends JpaRepository<Niveau, Long> {

	public Optional<Niveau> findByNiveauIdEquals(String niveauId);

	@Query(value = "SELECT max(ordre) FROM Niveau")
	public Integer findMaxOrdre();

	@Query(value = "SELECT min(ordre) FROM Niveau")
	public Integer findMinOrdre();

	public Optional<Niveau> findByOrdre(int i);

}
