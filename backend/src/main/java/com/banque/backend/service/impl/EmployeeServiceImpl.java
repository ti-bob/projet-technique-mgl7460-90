package com.banque.backend.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.banque.backend.controller.exception.NotFoundException;
import com.banque.backend.controller.exception.UniqueConstraintException;
import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Niveau;
import com.banque.backend.repository.ClientProduitRepository;
import com.banque.backend.repository.ClientRepository;
import com.banque.backend.repository.NiveauRepository;
import com.banque.backend.repository.ProduitRepository;
import com.banque.backend.service.EmployeeService;
import com.banque.backend.utilitaire.Statut;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	private final ClientRepository clientRepository;
	private final NiveauRepository niveauRepository;
	private final ProduitRepository produitRepository;
	private final ClientProduitRepository clientProduitRepository;

	@Override
	public Client addClient(String nom) {
		log.info("Ajout Client {}", nom);
		Optional<Niveau> niveau = niveauRepository.findByNiveauIdEquals("N1");
		if (!niveau.isPresent()) {
			throw NotFoundException.createWith("Niveau 'N1'");
		}

		Optional<Client> client = clientRepository.findByNomEquals(nom);
		if (client.isPresent()) {
			throw UniqueConstraintException.createWith("Client '" + nom + "'");
		}

		Client clientAdd = new Client(nom, niveau.get());

		Client returnclient = null;
		returnclient = clientRepository.saveAndFlush(clientAdd);

		return returnclient;
	}

	@Override
	public Client changeNiveauClient(@Valid String clientNom, boolean upgrade) {
		log.info("Change niveau Client {}, action {}", clientNom, upgrade ? "Up" : "Down");
		Client client = findByNomEquals(clientNom);
		@NonNull
		Niveau ActuelNiveau = client.getNiveau();
		Optional<Niveau> nouveauNiveau;
		if (upgrade) {
			if (ActuelNiveau.getOrdre() < niveauRepository.findMaxOrdre()) {
				nouveauNiveau = niveauRepository.findByOrdre(ActuelNiveau.getOrdre() + 1);
				if (nouveauNiveau.isPresent()) {
					client.setNiveau(nouveauNiveau.get());
					clientRepository.save(client);
				}
			}
		} else {
			if (ActuelNiveau.getOrdre() > niveauRepository.findMinOrdre()) {
				nouveauNiveau = niveauRepository.findByOrdre(ActuelNiveau.getOrdre() - 1);
				if (nouveauNiveau.isPresent()) {
					client.setNiveau(nouveauNiveau.get());
					clientRepository.save(client);
				}
			}
		}

		return client;
	}

	private Client findByNomEquals(String nom) {
		Optional<Client> client = clientRepository.findByNomEquals(nom);
		if (!client.isPresent()) {
			throw NotFoundException.createWith("Client '" + nom + "'");
		}
		return client.get();
	}

	@Override
	public Set<ClientProduit> listClientProduits(String clientNom) {
		log.info("listClientProduits Client {}", clientNom);

		Client client = findByNomEquals(clientNom);
		Set<ClientProduit> produits = client.getProduits();

		return produits;
	}

	@Override
	public List<ClientProduit> listClientsProduits(@NotNull @Valid Boolean aValider) {
		if (aValider) {
			Set<Statut> statuts = new HashSet<Statut>();
			statuts.add(Statut.EN_ATTENTE_AJOUT);
			statuts.add(Statut.EN_ATTENTE_SUPRESSION);

			return clientProduitRepository.findAllByStatutInOrderByClientAscProduitNiveauAsc(statuts);
		} else {
			return clientProduitRepository.findAll();
		}
	}

	@Override
	public ClientProduit setDemandeClientProsdui(String clientNom, String produitId, boolean accept) {
		log.info("setDeamndeClientProsdui Client {} ProduitID {} accept {}", clientNom, produitId, accept);
		ClientProduit clientProduit = clientProduitRepository.findByClientNomAndProduitProduitId(clientNom, produitId);

		if (ObjectUtils.isNotEmpty(clientProduit) && (clientProduit.getStatut().equals(Statut.EN_ATTENTE_AJOUT)
				|| clientProduit.getStatut().equals(Statut.EN_ATTENTE_SUPRESSION))) {
			if (accept) {
				clientProduit.setStatut(Statut.APPROUVE);
			} else {
				clientProduit.setStatut(Statut.REFUSE);
			}
			return clientProduitRepository.saveAndFlush(clientProduit);
		}
		return clientProduit;
	}

}
