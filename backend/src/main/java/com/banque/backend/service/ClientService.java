package com.banque.backend.service;

import java.util.List;

import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Produit;

public interface ClientService {

	ClientProduit abonnerProduit(String clientNom, String produitID);

	ClientProduit desabonnerProduit(String clientNom, String produitID);

	Client findByNomEquals(String nom);

	List<Produit> listProduitByClientNomEquals(String nom);

	List<Produit> listProduitDispoClientNomEquals(String nom);

}
