package com.banque.backend.service;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;

public interface EmployeeService {

	Client addClient(@Valid String body);

	Client changeNiveauClient(@Valid String clientNom, boolean b);

	Set<ClientProduit> listClientProduits(String clientNom);

	List<ClientProduit> listClientsProduits(@NotNull @Valid Boolean aValider);

	ClientProduit setDemandeClientProsdui(String clientNom, String produitId, boolean accept);

}
