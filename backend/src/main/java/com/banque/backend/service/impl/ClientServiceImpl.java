package com.banque.backend.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.banque.backend.controller.exception.NotFoundException;
import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Produit;
import com.banque.backend.repository.ClientProduitRepository;
import com.banque.backend.repository.ClientRepository;
import com.banque.backend.repository.NiveauRepository;
import com.banque.backend.repository.ProduitRepository;
import com.banque.backend.service.ClientService;
import com.banque.backend.utilitaire.Approbation;
import com.banque.backend.utilitaire.Statut;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class ClientServiceImpl implements ClientService {

	private final ClientRepository clientRepository;
	private final NiveauRepository niveauRepository;
	private final ProduitRepository produitRepository;
	private final ClientProduitRepository clientProduitRepository;

	@Override
	public ClientProduit abonnerProduit(String clientNom, String produitID) {
		log.info("Abonner produit Client {} produit {}", clientNom, produitID);

		Client client = findByNomEquals(clientNom);
		Produit produit = findByProduitIdEquals(produitID);
		ClientProduit returnClientProduit = null;
		Optional<ClientProduit> clientProduit = clientProduitRepository.findByClientAndProduitEquals(client, produit);
		if (clientProduit.isEmpty()) {
			log.debug("ajout clientProduit");

			if (client.getNiveau().getOrdre() >= produit.getNiveau().getOrdre()) {
				ClientProduit addClientProduit = new ClientProduit(client, produit);
				if (produit.getApprobationRequise().compareTo(Approbation.SUBSCRIBE) == 0
						|| produit.getApprobationRequise().compareTo(Approbation.BOTH) == 0) {
					addClientProduit.setStatut(Statut.EN_ATTENTE_AJOUT);
				} else {
					addClientProduit.setStatut(Statut.APPROUVE);
				}
				client.getProduits().add(addClientProduit);
				produit.getClients().add(addClientProduit);
				returnClientProduit = clientProduitRepository.saveAndFlush(addClientProduit);
			}
//TODO: Ajouter message quand le client n'a pas le niveau nécessaire
		} else {
			log.debug("clientProduit {}", clientProduit);
			returnClientProduit = clientProduit.get();
		}
		return returnClientProduit;
	}

	@Override
	public ClientProduit desabonnerProduit(String clientNom, String produitID) {
		log.info("Désabonner produit Client {} produit {}", clientNom, produitID);

		Client client = findByNomEquals(clientNom);
		Produit produit = findByProduitIdEquals(produitID);
		ClientProduit returnClientProduit = null;
		Optional<ClientProduit> clientProduit = clientProduitRepository.findByClientAndProduitEquals(client, produit);
		if (clientProduit.isPresent()) {
			ClientProduit cp = clientProduit.get();
			log.debug("clientProduit {}", cp);
			if (cp.getStatut().compareTo(Statut.EN_ATTENTE_SUPRESSION) == 0) {
				returnClientProduit = cp;
			} else if (cp.getProduit().getApprobationRequise().compareTo(Approbation.UNSUBSCRIBE) == 0
					|| cp.getProduit().getApprobationRequise().compareTo(Approbation.BOTH) == 0) {
				cp.setStatut(Statut.EN_ATTENTE_SUPRESSION);
				returnClientProduit = clientProduitRepository.saveAndFlush(cp);
			} else {
				clientProduitRepository.delete(cp);

				client.getProduits().remove(cp);
				produit.getClients().remove(cp);
			}
		}
		return returnClientProduit;
	}

	@Override
	public Client findByNomEquals(String nom) {
		Optional<Client> client = clientRepository.findByNomEquals(nom);
		if (!client.isPresent()) {
			throw NotFoundException.createWith("Client '" + nom + "'");
		}
		return client.get();
	}

	private Produit findByProduitIdEquals(String produitId) {
		Optional<Produit> produit = produitRepository.findByProduitIdEquals(produitId);
		if (!produit.isPresent()) {
			throw NotFoundException.createWith("Produit '" + produitId + "'");
		}
		return produit.get();
	}

	@Override
	public List<Produit> listProduitByClientNomEquals(String nom) {

		Client client = findByNomEquals(nom);

		List<ClientProduit> cp = client.getProduits().stream().collect(Collectors.toList());
		List<Produit> produits = new ArrayList<Produit>();
		for (ClientProduit x : cp) {
			produits.add(x.getProduit());
		}

		return produits;
	}

	@Override
	public List<Produit> listProduitDispoClientNomEquals(String nom) {
		Client client = findByNomEquals(nom);

		int ordre = client.getNiveau().getOrdre();
		return produitRepository.findProduitNiveauQuery(ordre);
	}

}
