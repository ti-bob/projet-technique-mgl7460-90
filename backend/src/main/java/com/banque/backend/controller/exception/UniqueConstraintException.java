package com.banque.backend.controller.exception;

public class UniqueConstraintException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String objectName;

	public static UniqueConstraintException createWith(String objectName) {
		return new UniqueConstraintException(objectName);
	}

	private UniqueConstraintException(String objectName) {
		this.objectName = objectName;
	}

	@Override
	public String getMessage() {
		return objectName + " existe déja";
	}
}
