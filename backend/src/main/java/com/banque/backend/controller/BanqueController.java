package com.banque.backend.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.banque.contrat.api.serveur.banque.StatusApi;

@RestController
public class BanqueController implements StatusApi {

	@Override
	public ResponseEntity<String> getStatus() {
		String retval = new String("I'm alive");
		return new ResponseEntity<>(retval, HttpStatus.OK);
	}

}
