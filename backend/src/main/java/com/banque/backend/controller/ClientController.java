package com.banque.backend.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Produit;
import com.banque.backend.service.ClientService;
import com.banque.contrat.api.serveur.client.ClientApi;
import com.banque.contrat.api.serveur.client.model.ClientProduitDto;

@RestController
public class ClientController implements ClientApi {

	@Autowired
	private ClientService clientService;

	@Override
	public ResponseEntity<List<ClientProduitDto>> getClientProduits(String clientNom, @Valid Boolean souscrit) {
		List<Produit> produits = null;
		List<ClientProduitDto> clientProduitDtos = new ArrayList<>();

		if (StringUtils.isNotBlank(clientNom)) {
			if (souscrit) {
				produits = clientService.listProduitByClientNomEquals(clientNom);
			} else {
				produits = clientService.listProduitDispoClientNomEquals(clientNom);
			}
			for (Produit p : produits) {
				ClientProduitDto cpd = new ClientProduitDto();
				cpd.setProduitId(p.getProduitId());
				cpd.setNom(p.getNom());
				clientProduitDtos.add(cpd);
			}
		}
		return new ResponseEntity<>(clientProduitDtos, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ClientProduitDto> subscribeProduits(String clientNom, String produitID) {
		ClientProduit clientProduit = clientService.abonnerProduit(clientNom, produitID);
		ClientProduitDto clientProduitDto = new ClientProduitDto();
		if (clientProduit != null) {
			clientProduitDto.setProduitId(clientProduit.getProduit().getProduitId());
			clientProduitDto.setNom(clientProduit.getProduit().getNom());
			clientProduitDto.setStatut(clientProduit.getStatut().name());
		}
		return new ResponseEntity<>(clientProduitDto, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ClientProduitDto> unsubscribeProduits(String clientNom, String produitID) {
		ClientProduit clientProduit = clientService.desabonnerProduit(clientNom, produitID);
		ClientProduitDto clientProduitDto = new ClientProduitDto();
		if (clientProduit != null) {
			clientProduitDto.setProduitId(clientProduit.getProduit().getProduitId());
			clientProduitDto.setNom(clientProduit.getProduit().getNom());
			clientProduitDto.setStatut(clientProduit.getStatut().name());
		}
		return new ResponseEntity<>(clientProduitDto, HttpStatus.OK);
	}

}
