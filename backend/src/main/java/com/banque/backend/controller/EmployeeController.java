package com.banque.backend.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.banque.backend.entity.Client;
import com.banque.backend.entity.ClientProduit;
import com.banque.backend.entity.Niveau;
import com.banque.backend.entity.Produit;
import com.banque.backend.service.EmployeeService;
import com.banque.contrat.api.client.employee.model.ActionDemandeProduitDto;
import com.banque.contrat.api.serveur.employee.EmployeeApi;
import com.banque.contrat.api.serveur.employee.model.ClientDto;
import com.banque.contrat.api.serveur.employee.model.ClientProduitDto;
import com.banque.contrat.api.serveur.employee.model.ClientProduitsDto;
import com.banque.contrat.api.serveur.employee.model.DemandeClientProduitDto;
import com.banque.contrat.api.serveur.employee.model.InlineObjectDto;
import com.banque.contrat.api.serveur.employee.model.NiveauDto;
import com.banque.contrat.api.serveur.employee.model.ProduitDto;

import lombok.NonNull;

@RestController
public class EmployeeController implements EmployeeApi {

	@Autowired
	private EmployeeService employeeService;

	@Override
	public ResponseEntity<ClientDto> ajoutClient(@Valid String body) {
		Client savedClient = employeeService.addClient(body);
		ClientDto clientreturn = new ClientDto();
		clientreturn.setNom(savedClient.getNom());
		clientreturn.setId(savedClient.getId());
		@NonNull
		Niveau niveau = savedClient.getNiveau();
		NiveauDto niveauDto = new NiveauDto();
		niveauDto.setId(niveau.getNiveauId());
		niveauDto.setDescription(niveau.getDescription());
		clientreturn.setNiveau(niveauDto);

		return ResponseEntity.status(HttpStatus.CREATED).body(clientreturn);
	}

	@Override
	public ResponseEntity<ClientDto> changeNiveauClient(@Valid InlineObjectDto jsonBody) {
		Client updatedClient = null;

		switch (jsonBody.getAction()) {
		case UPGRADE:
			updatedClient = employeeService.changeNiveauClient(jsonBody.getClientNom(), true);
			break;
		case DOWNGRADE:
			updatedClient = employeeService.changeNiveauClient(jsonBody.getClientNom(), false);
			break;
		default:
			throw new IllegalArgumentException("L'action : " + jsonBody.getAction() + " est invalide");
		}
		ClientDto clientreturn = new ClientDto();
		clientreturn.setNom(updatedClient.getNom());
		clientreturn.setId(updatedClient.getId());
		@NonNull
		Niveau niveau = updatedClient.getNiveau();
		NiveauDto niveauDto = new NiveauDto();
		niveauDto.setId(niveau.getNiveauId());
		niveauDto.setDescription(niveau.getDescription());
		clientreturn.setNiveau(niveauDto);

		return ResponseEntity.ok().body(clientreturn);

	}

	@Override
	public ResponseEntity<ClientProduitsDto> getClientProduits(String clientNom) {
		Set<ClientProduit> produits = null;

		produits = employeeService.listClientProduits(clientNom);

		Iterator<ClientProduit> itr = produits.iterator();
		ClientProduitsDto clientProduitsDto = new ClientProduitsDto();
		if (itr.hasNext()) {
			ClientProduit cp = itr.next();
			ClientDto client = new ClientDto();
			client.setId(cp.getClient().getId());
			client.setNom(cp.getClient().getNom());
			NiveauDto niveauDto = new NiveauDto();
			niveauDto.setId(cp.getClient().getNiveau().getNiveauId());
			niveauDto.setDescription(cp.getClient().getNiveau().getDescription());
			client.setNiveau(niveauDto);
			clientProduitsDto.setClient(client);

			ProduitDto produitDto = new ProduitDto();
			produitDto.setId(cp.getProduit().getProduitId());
			produitDto.setNom(cp.getProduit().getNom());
			niveauDto = new NiveauDto();
			niveauDto.setId(cp.getProduit().getNiveau().getNiveauId());
			niveauDto.setDescription(cp.getProduit().getNiveau().getDescription());
			produitDto.setNiveau(niveauDto);
			clientProduitsDto.addProduitsItem(produitDto);
			while (itr.hasNext()) {
				cp = itr.next();
				produitDto = new ProduitDto();
				produitDto.setId(cp.getProduit().getProduitId());
				produitDto.setNom(cp.getProduit().getNom());
				niveauDto = new NiveauDto();
				niveauDto.setId(cp.getProduit().getNiveau().getNiveauId());
				niveauDto.setDescription(cp.getProduit().getNiveau().getDescription());
				produitDto.setNiveau(niveauDto);
				clientProduitsDto.addProduitsItem(produitDto);

			}
		}

		return ResponseEntity.ok().body(clientProduitsDto);

	}

	@Override
	public ResponseEntity<List<ClientProduitsDto>> getClientsProduits(@NotNull @Valid Boolean aValider) {
		List<ClientProduit> listClientProduit = employeeService.listClientsProduits(aValider);
		List<ClientProduitsDto> retour = new ArrayList<ClientProduitsDto>();

		loopClientProduit: for (ClientProduit clientProduit : listClientProduit) {
			Client client = clientProduit.getClient();
			Produit produit = clientProduit.getProduit();

			for (ClientProduitsDto cpd : retour) {
				if (cpd.getClient().getNom().equals(client.getNom())) {
					ProduitDto produitDto = new ProduitDto().id(produit.getProduitId()).nom(produit.getNom())
							.niveau(new NiveauDto().id(produit.getNiveau().getNiveauId())
									.description(produit.getNiveau().getDescription()));

					cpd.addProduitsItem(produitDto);
					continue loopClientProduit;
				}
			}

			ClientProduitsDto cpd = new ClientProduitsDto();
			cpd.client(new ClientDto().id(client.getId()).nom(client.getNom()).niveau(new NiveauDto()
					.id(client.getNiveau().getNiveauId()).description(client.getNiveau().getDescription())));
			cpd.addProduitsItem(new ProduitDto().id(produit.getProduitId()).nom(produit.getNom()).niveau(new NiveauDto()
					.id(produit.getNiveau().getNiveauId()).description(produit.getNiveau().getDescription())));
			retour.add(cpd);

		}

		return ResponseEntity.ok().body(retour);
	}

	@Override
	public ResponseEntity<ClientProduitDto> setDeamndeClientProsdui(
			@Valid DemandeClientProduitDto demandeClientProduitDto)

	{
		ClientProduit clientProduit = employeeService.setDemandeClientProsdui(demandeClientProduitDto.getNom(),
				demandeClientProduitDto.getProduitId(),
				demandeClientProduitDto.getAction().name().equals(ActionDemandeProduitDto.ACCEPT.name()));

		ClientProduitDto clientProduitDto = new ClientProduitDto();
		ClientDto client = new ClientDto();
		if (ObjectUtils.isNotEmpty(clientProduit)) {
			client.setId(clientProduit.getClient().getId());
			client.setNom(clientProduit.getClient().getNom());
			NiveauDto niveauDto = new NiveauDto();
			niveauDto.setId(clientProduit.getClient().getNiveau().getNiveauId());
			niveauDto.setDescription(clientProduit.getClient().getNiveau().getDescription());
			client.setNiveau(niveauDto);
			clientProduitDto.setClient(client);

			ProduitDto produitDto = new ProduitDto();
			produitDto.setId(clientProduit.getProduit().getProduitId());
			produitDto.setNom(clientProduit.getProduit().getNom());
			niveauDto = new NiveauDto();
			niveauDto.setId(clientProduit.getProduit().getNiveau().getNiveauId());
			niveauDto.setDescription(clientProduit.getProduit().getNiveau().getDescription());
			produitDto.setNiveau(niveauDto);
			clientProduitDto.setProduits(produitDto);

			clientProduitDto.setStatut(clientProduit.getStatut().name());
		}
		return ResponseEntity.ok().body(clientProduitDto);
	}

}
