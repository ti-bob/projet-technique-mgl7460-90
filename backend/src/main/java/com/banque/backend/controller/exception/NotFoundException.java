package com.banque.backend.controller.exception;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String objectName;

	public static NotFoundException createWith(String objectName) {
		return new NotFoundException(objectName);
	}

	private NotFoundException(String objectName) {
		this.objectName = objectName;
	}

	@Override
	public String getMessage() {
		return objectName + " non trouvé";
	}
}
