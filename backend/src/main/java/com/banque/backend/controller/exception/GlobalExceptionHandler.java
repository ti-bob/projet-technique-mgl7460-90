package com.banque.backend.controller.exception;

import java.util.Collections;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import com.banque.contrat.api.serveur.banque.model.ErrorResponseDto;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	@ExceptionHandler({ NotFoundException.class, UniqueConstraintException.class })
	@Nullable
	public final ResponseEntity<ErrorResponseDto> handleException(Exception ex, WebRequest request) {
		HttpHeaders headers = new HttpHeaders();

		log.error("Handling " + ex.getClass().getSimpleName() + " due to " + ex.getMessage());

		if (ex instanceof NotFoundException) {
			HttpStatus status = HttpStatus.NOT_FOUND;
			NotFoundException nfe = (NotFoundException) ex;

			return handleUserNotFoundException(nfe, headers, status, request);
		} else if (ex instanceof UniqueConstraintException) {
			HttpStatus status = HttpStatus.CONFLICT;
			UniqueConstraintException uce = (UniqueConstraintException) ex;

			return handleUniqueConstraintException(uce, headers, status, request);
		} else {
			if (log.isWarnEnabled()) {
				log.warn("Unknown exception type: " + ex.getClass().getName());
			}

			HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
			return handleExceptionInternal(ex, null, headers, status, request);
		}
	}

	protected ResponseEntity<ErrorResponseDto> handleUserNotFoundException(NotFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		List<String> errors = Collections.singletonList(ex.getMessage());
		return handleExceptionInternal(ex, new ErrorResponseDto().code(status.name()).message(ex.getMessage()), headers,
				status, request);
	}

	protected ResponseEntity<ErrorResponseDto> handleUniqueConstraintException(UniqueConstraintException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> errors = Collections.singletonList(ex.getMessage());
		return handleExceptionInternal(ex, new ErrorResponseDto().code(status.name()).message(ex.getMessage()), headers,
				status, request);
	}

	protected ResponseEntity<ErrorResponseDto> handleExceptionInternal(Exception ex, @Nullable ErrorResponseDto body,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
			request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
		}

		return new ResponseEntity<ErrorResponseDto>(body, headers, status);
	}
}
