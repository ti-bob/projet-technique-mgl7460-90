package com.banque.backend.utilitaire;

public enum Statut {
	APPROUVE, EN_ATTENTE_AJOUT, EN_ATTENTE_SUPRESSION, REFUSE;
}
