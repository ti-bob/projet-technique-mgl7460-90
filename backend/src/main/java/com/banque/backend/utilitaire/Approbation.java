package com.banque.backend.utilitaire;

public enum Approbation {
	NONE, SUBSCRIBE, UNSUBSCRIBE, BOTH;
}
