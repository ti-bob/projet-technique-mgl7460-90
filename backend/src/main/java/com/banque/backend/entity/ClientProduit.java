package com.banque.backend.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.banque.backend.utilitaire.Statut;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@Data
@Table(name = "tbl_client_produit", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "client_id", "produit_id" }) })
public class ClientProduit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.PROTECTED)
	private Long id;

	@NotNull
	@NonNull
	@ManyToOne
	@JoinColumn(name = "client_id")
	private Client client;

	@NotNull
	@NonNull
	@ManyToOne
	@JoinColumn(name = "produit_id")
	private Produit produit;

	@Enumerated(EnumType.STRING)
	private Statut statut;

}
