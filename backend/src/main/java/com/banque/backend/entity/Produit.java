package com.banque.backend.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.banque.backend.utilitaire.Approbation;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@Data
@Table(name = "tbl_produit")
public class Produit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.PROTECTED)
	private Long id;

	@NotNull
	@NonNull
	private String produitId;

	@NotNull
	@NonNull
	private String nom;

	@NotNull
	@NonNull
	@Enumerated(EnumType.STRING)
	private Approbation approbationRequise;

	@EqualsAndHashCode.Exclude
	@OneToMany(mappedBy = "produit")
	Set<ClientProduit> clients;

	@NotNull
	@OneToOne
	@JoinColumn(name = "nid")
	@NonNull
	private Niveau niveau;

	public void addClientProduit(ClientProduit clientProduit) {
		this.clients.add(clientProduit);
	}
}
