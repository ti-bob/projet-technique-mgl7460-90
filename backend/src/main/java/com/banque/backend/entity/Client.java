package com.banque.backend.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@Data
@Table(name = "tbl_client", uniqueConstraints = { @UniqueConstraint(columnNames = { "nom" }) })
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.PROTECTED)
	private Long id;

	@NotNull
	@NonNull
	private String nom;

	@EqualsAndHashCode.Exclude
	@OneToMany(mappedBy = "client")
	Set<ClientProduit> produits;

	@NotNull
	@OneToOne
	@JoinColumn(name = "nid")
	@NonNull
	private Niveau niveau;

	public void addProduit(Produit produit) {
		ClientProduit cp = new ClientProduit(this, produit);
		this.produits.add(cp);
		produit.getClients().add(cp);
	}
}
