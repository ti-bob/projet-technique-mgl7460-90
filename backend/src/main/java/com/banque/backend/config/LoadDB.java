package com.banque.backend.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.banque.backend.entity.Niveau;
import com.banque.backend.entity.Produit;
import com.banque.backend.repository.NiveauRepository;
import com.banque.backend.repository.ProduitRepository;
import com.banque.backend.utilitaire.Approbation;

@Configuration
public class LoadDB {

	@Bean
	CommandLineRunner initDatabaseNiveau(ProduitRepository pr, NiveauRepository npr) {
		return args -> {
			Niveau np1 = new Niveau("N1", "Pauvre", 1);
			Niveau np2 = new Niveau("N2", "Classe moyenne", 2);
			Niveau np3 = new Niveau("N3", "Aisé", 3);
			Niveau np4 = new Niveau("N4", "Fait parti du 1%", 4);
			npr.save(np1);
			npr.save(np2);
			npr.save(np3);
			npr.save(np4);
			pr.save(new Produit("PE1", "Epargne 0,1%", Approbation.NONE, np1));
			pr.save(new Produit("PE2", "Epargne 2,0%", Approbation.SUBSCRIBE, np2));
			pr.save(new Produit("PE3", "Epargne 4,0%", Approbation.SUBSCRIBE, np3));
			pr.save(new Produit("PE4", "Epargne 10,0%", Approbation.BOTH, np4));
			pr.save(new Produit("PF4", "Financement 0,1%", Approbation.BOTH, np4));
			pr.save(new Produit("PF3", "Financement 2,0%", Approbation.BOTH, np3));
			pr.save(new Produit("PF2", "Financement 4,0%", Approbation.UNSUBSCRIBE, np2));
			pr.save(new Produit("PF1", "Financement 10,0%", Approbation.UNSUBSCRIBE, np1));
		};
	}

}
