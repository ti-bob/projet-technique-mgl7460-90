## Projet Technique MGL7460-90 (Hiver 2020)

* Collaborateurs
    * David Lafrance
    * Gerry O'Meara 
##



## compilation et installation du projet 
* dans un shell bash
    * pour cloner le projet sur votre poste
        * git clone https://gitlab.com/ti-bob/projet-technique-mgl7460-90.git
    * pour compiler et installer le projet
        * ./mvnw clean install   


* Visualisation du rappour de test Serenity
    * Dans le module backend se rendre dans "backend/target/site/serenity/index.html"
##     
## utilisation des scripts
* dans un shell bash
    * cd script

### fonction pour démarrer la banque   
 
| fonction              | commande bash               | 
| :------:               | :-----                | 
| demarrer la banque              | ./banque  | 

### fonction reliée a un employee   
 
| fonction             | commande bash               | 
| :------:               | :-----                | 
| ajouter un client             | ./employee --add  < Nom client> | 
| Liste les produits attaché au compte de ce client| ./employee --list CLIENT_NAME|
| Accepte la mise en place du produit pour le client| ./employee --accept PRODUCT_ID --client CLIENT_NAME |
| Rejette le produit demandé par le client| ./employee --reject PRODUCT_ID --client CLIENT_NAME|
| Liste les clients avec des produits en attente de validation| ./employee --tasks |
| Augmente le statut du client, qui accède à de nouveaux produits|./employee --upgrade CLIENT_NAME|
| Diminue le statut du client, restreignant les produits auxquel il a accès| ./employee --downgrade CLIENT_NAME|

### fonction reliée a un client   
 
| fonction              |  commande bash               | 
| :------:               | :-----                | 
| Liste tous les produits du client| ./client -n CLIENT_NAME --status   | 
| Liste tous les produits auxquel le client à accès| ./client -n CLIENT_NAME --avail  | 
| Souscrit à un produit| ./client -n CLIENT_NAME --subscribe PRODUCT_ID  | 
| Quitte un produit| ./client -n CLIENT_NAME --unsubscribe PRODUCT_ID  | 


