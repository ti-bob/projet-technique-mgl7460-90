package com.banque.employee.service;

import java.util.List;

import com.banque.contrat.api.client.employee.model.ClientDto;
import com.banque.contrat.api.client.employee.model.ClientProduitDto;
import com.banque.contrat.api.client.employee.model.ClientProduitsDto;

public interface BanqueService {

	public ClientProduitDto accepteClientProduit(String clientNom, String ProduitId);

	public ClientDto ajoutClient(String clientNom);

	public ClientDto downgradeClient(String downgradeClientNom);

	public List<ClientProduitsDto> getListeClientsPtoduitsAttente();

	public ClientProduitsDto getListeProduitsClient(String clientNom);

	public ClientProduitDto rejectClientProduit(String clientNom, String ProduitId);

	public ClientDto upgradeClient(String clientNom);
}
