package com.banque.employee.service.impl;

import java.net.ConnectException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

import com.banque.contrat.api.client.employee.EmployeeApi;
import com.banque.contrat.api.client.employee.model.ActionClientDto;
import com.banque.contrat.api.client.employee.model.ActionDemandeProduitDto;
import com.banque.contrat.api.client.employee.model.ClientDto;
import com.banque.contrat.api.client.employee.model.ClientProduitDto;
import com.banque.contrat.api.client.employee.model.ClientProduitsDto;
import com.banque.contrat.api.client.employee.model.DemandeClientProduitDto;
import com.banque.contrat.api.client.employee.model.ErrorResponseDto;
import com.banque.contrat.api.client.employee.model.InlineObjectDto;
import com.banque.employee.service.BanqueService;
import com.google.gson.Gson;

@Service
public class BanqueServiceImpl implements BanqueService {

	private EmployeeApi employeeApi;

	@Autowired
	BanqueServiceImpl(EmployeeApi employeeApi) {
		this.employeeApi = employeeApi;
	}

	@Override
	public ClientProduitDto accepteClientProduit(String clientNom, String ProduitId) {

		return setDemandeClientProduit(clientNom, ProduitId, true);

	}

	@Override
	public ClientDto ajoutClient(String clientNom) {
		ClientDto clientDto = null;

		try {
			clientDto = employeeApi.ajoutClient(clientNom);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				// handle connect exception
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return clientDto;
	}

	private ClientDto changeNiveauClient(String clientNom, ActionClientDto action) {
		ClientDto clientDto = null;
		InlineObjectDto jsonBody = new InlineObjectDto().clientNom(clientNom).action(action);

		try {
			clientDto = employeeApi.changeNiveauClient(jsonBody);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				// handle connect exception
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return clientDto;
	}

	@Override
	public ClientDto downgradeClient(String clientNom) {
		return changeNiveauClient(clientNom, ActionClientDto.DOWNGRADE);
	}

	@Override
	public List<ClientProduitsDto> getListeClientsPtoduitsAttente() {
		List<ClientProduitsDto> clientProduitsDto = null;
		try {
			clientProduitsDto = employeeApi.getClientsProduits(true);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return clientProduitsDto;

	}

	@Override
	public ClientProduitsDto getListeProduitsClient(String clientNom) {
		ClientProduitsDto clientProduitsDto = null;
		try {
			clientProduitsDto = employeeApi.getClientProduits(clientNom);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return clientProduitsDto;

	}

	@Override
	public ClientProduitDto rejectClientProduit(String clientNom, String ProduitId) {
		return setDemandeClientProduit(clientNom, ProduitId, false);

	}

	private ClientProduitDto setDemandeClientProduit(String clientNom, String produitId, boolean accept) {
		DemandeClientProduitDto demandeClientProduitDto = new DemandeClientProduitDto().nom(clientNom)
				.produitId(produitId);
		if (accept) {
			demandeClientProduitDto.setAction(ActionDemandeProduitDto.ACCEPT);
		} else {
			demandeClientProduitDto.setAction(ActionDemandeProduitDto.REJECT);
		}

		ClientProduitDto clientProduitDto = null;
		try {
			clientProduitDto = employeeApi.setDeamndeClientProsdui(demandeClientProduitDto);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return clientProduitDto;

	}

	@Override
	public ClientDto upgradeClient(String clientNom) {
		return changeNiveauClient(clientNom, ActionClientDto.UPGRADE);
	}

}
