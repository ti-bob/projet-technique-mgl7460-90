package com.banque.employee.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.banque.contrat.api.client.employee.EmployeeApi;
import com.banque.contrat.api.client.employee.handler.ApiClient;

@Configuration
public class EmployeeConfig {
	@Bean
	public EmployeeApi employeeApi() {
		return new EmployeeApi(apiClient());
	}

	@Bean
	public ApiClient apiClient() {
		return new ApiClient();
	}
}
