package com.banque.employee;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.banque.contrat.api.client.employee.model.ClientDto;
import com.banque.contrat.api.client.employee.model.ClientProduitDto;
import com.banque.contrat.api.client.employee.model.ClientProduitsDto;
import com.banque.contrat.api.client.employee.model.ProduitDto;
import com.banque.employee.service.BanqueService;

import lombok.Getter;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Component
@Command(name = "employee")
public class EmployeeCommand implements Callable<Integer> {

	@Autowired
	private BanqueService banqueService;

	// Prevent "Unknown option" error when users use
	// the Spring Boot parameter 'spring.config.location' to specify
	// an alternative location for the application.properties file.
	@Option(names = "--spring.config.location", hidden = true)
	private String springConfigLocation;

	@ArgGroup(exclusive = true, multiplicity = "1")
	Exclusive exclusive;

	@Getter
	static class Exclusive {
		@Option(names = { "--add" }, paramLabel = "CLIENT_NAME", description = "Crée un client")
		private String addClient = "";
		@Option(names = { "--upgrade" }, paramLabel = "CLIENT_NAME", description = "Augmente le statut du client")
		private String upgradeClientNom = "";
		@Option(names = { "--downgrade" }, paramLabel = "CLIENT_NAME", description = "Diminue le statut du client")
		private String downgradeClientNom = "";
		@Option(names = {
				"--list" }, paramLabel = "CLIENT_NAME", description = "Liste les produits attaché au compte de ce client")
		private String clientNomListProduitClient = "";
		@Option(names = { "--tasks" }, description = "Liste les clients avec des produits en attente de validation")
		private boolean tasks;
		@ArgGroup(exclusive = false, multiplicity = "1")
		private ClientGroup clientGroup;

	}

	@Getter
	static class ClientGroup {
		@Option(names = "--client", paramLabel = "CLIENT_NAME", description = "Le nom du client", required = true)
		private String client = "";

		@ArgGroup(exclusive = true, multiplicity = "1")
		private Action action;

		@Getter
		static class Action {
			@Option(names = "--accept", paramLabel = "PRODUCT_ID", description = "L'Id du produit a accepter", required = true)
			private String acceptProduit = "";
			@Option(names = "--reject", paramLabel = "PRODUCT_ID", description = "L'Id du produit a refuser", required = true)
			private String rejectProduit = "";
		}
	}

	@Override
	public Integer call() throws Exception {
		ClientDto client = null;

		if (StringUtils.isNotBlank(exclusive.addClient)) {
			client = banqueService.ajoutClient(exclusive.addClient);
			if (client != null) {
				System.out.println("Client ajouté");

				afficheClient(client);
			}
		} else if (StringUtils.isNotBlank(exclusive.upgradeClientNom)) {
			client = banqueService.upgradeClient(exclusive.upgradeClientNom);
			if (client != null) {
				System.out.println("Client augmenté");

				afficheClient(client);
			}
		} else if (StringUtils.isNotBlank(exclusive.downgradeClientNom)) {
			client = banqueService.downgradeClient(exclusive.downgradeClientNom);
			if (client != null) {
				System.out.println("Client diminué");

				afficheClient(client);
			}
		} else if (StringUtils.isNotBlank(exclusive.clientNomListProduitClient)) {
			ClientProduitsDto clientProduitsDto = banqueService
					.getListeProduitsClient(exclusive.clientNomListProduitClient);
			if (clientProduitsDto != null) {
				afficheClient(clientProduitsDto.getClient());
				afficheProduits(clientProduitsDto.getProduits());
			}
		} else if (exclusive.tasks) {
			List<ClientProduitsDto> clientProduitsDtos = banqueService.getListeClientsPtoduitsAttente();
			if (clientProduitsDtos != null && !clientProduitsDtos.isEmpty()) {
				for (ClientProduitsDto cp : clientProduitsDtos) {
					afficheClient(cp.getClient());
					afficheProduits(cp.getProduits());

				}
			}
		} else if (StringUtils.isNotBlank(exclusive.getClientGroup().getClient())) {
			ClientProduitDto clientProduitDto = null;
			if (StringUtils.isNotBlank(exclusive.getClientGroup().getAction().getRejectProduit())) {
				clientProduitDto = banqueService.rejectClientProduit(exclusive.getClientGroup().getClient(),
						exclusive.getClientGroup().getAction().getRejectProduit());
			} else if (StringUtils.isNotBlank(exclusive.getClientGroup().getAction().getAcceptProduit())) {
				clientProduitDto = banqueService.accepteClientProduit(exclusive.getClientGroup().getClient(),
						exclusive.getClientGroup().getAction().getAcceptProduit());

			}
			if (clientProduitDto != null) {
//				for (ClientProduitsDto cp : clientProduitsDtos) {
				afficheClient(clientProduitDto.getClient());
				afficheProduit(clientProduitDto.getProduits());

//				}
			}

		}

		return 42;
	}

	private void afficheProduit(ProduitDto produit) {

		if (produit != null) {
			System.out.println("ID\tNom\tNiveau");
			System.out.println(produit.getId() + "\t" + produit.getNom() + "\t" + produit.getNiveau().getId() + " - "
					+ produit.getNiveau().getDescription());
		}

	}

	private void afficheProduits(List<ProduitDto> produits) {

		if (produits != null && !produits.isEmpty()) {
			System.out.println("ID\tNom\tNiveau");
			for (ProduitDto p : produits) {
				System.out.println(p.getId() + "\t" + p.getNom() + "\t" + p.getNiveau().getId() + " - "
						+ p.getNiveau().getDescription());
			}
		}

	}

	private void afficheClient(ClientDto client) {
		if (client != null) {
			System.out.println("Client");
			System.out.println("   Id     : " + client.getId());
			System.out.println("   Nom    : " + client.getNom());
			System.out
					.println("   Niveau : " + client.getNiveau().getId() + " - " + client.getNiveau().getDescription());
		}
	}
}
