package com.banque.employee;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import picocli.CommandLine;
import picocli.CommandLine.IFactory;

@SpringBootApplication
public class EmployeeApp implements CommandLineRunner, ExitCodeGenerator {

	private IFactory factory; // auto-configured to inject PicocliSpringFactory
	private EmployeeCommand employeeCommand; // your @picocli.CommandLine.Command-annotated class
	private int exitCode;

	// constructor injection
	EmployeeApp(IFactory factory, EmployeeCommand myCommand) {
		this.factory = factory;
		this.employeeCommand = myCommand;
	}

	@Override
	public int getExitCode() {
		return exitCode;
	}

	@Override
	public void run(String... args) throws Exception {
		// let picocli parse command line args and run the business logic
		exitCode = new CommandLine(employeeCommand, factory).execute(args);
	}

	public static void main(String[] args) {
		// let Spring instantiate and inject dependencies
		System.exit(SpringApplication.exit(SpringApplication.run(EmployeeApp.class, args)));
	}
}
