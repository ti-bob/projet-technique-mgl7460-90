package com.banque.employee.service.impl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.banque.contrat.api.client.employee.EmployeeApi;
import com.banque.contrat.api.client.employee.model.ClientDto;

@ExtendWith(MockitoExtension.class)
class BanqueServiceImplTest {

	@Mock
	EmployeeApi employeeApi;

	private BanqueServiceImpl banqueServiceImpl;

	@BeforeEach
	void initUseCase() {
		banqueServiceImpl = new BanqueServiceImpl(employeeApi);
	}

	@Test
	void testAjoutClient() {
		ClientDto client = new ClientDto().id(1l).nom("UnClient");
		Mockito.when(employeeApi.ajoutClient(ArgumentMatchers.anyString())).thenReturn(client);
		ClientDto reponse = banqueServiceImpl.ajoutClient("UnClient");
		Assertions.assertThat(reponse).isEqualTo(client);
	}

//	@Test
//	void testAjoutClient_when_HttpClientErrorException() {
//		HttpClientErrorException expected = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "toto");
//
//		Mockito.when(employeeApi.ajoutClient(ArgumentMatchers.anyString())).thenThrow(expected);
//
//		ClientDto reponse = banqueServiceImpl.ajoutClient("UnClient");
//
//		Assertions.assertThat(reponse).isEqualTo(expected);
//	}

}
