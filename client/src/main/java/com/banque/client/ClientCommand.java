package com.banque.client;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.banque.client.service.BanqueService;
import com.banque.contrat.api.client.client.model.ClientProduitDto;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;
import lombok.Getter;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Component
@Command(name = "client", sortOptions = false)
public class ClientCommand implements Callable<Integer> {

	@Autowired
	private BanqueService banqueService;

	@Option(names = { "-n" }, required = true, paramLabel = "CLIENT_NAME", description = "nom du client")
	private String clientNom;

	@ArgGroup(exclusive = true, multiplicity = "1")
	Exclusive exclusive;

	@Getter
	static class Exclusive {
		@Option(names = {
				"--avail" }, required = true, description = "Liste tous les produits auxquel le client à accès")
		boolean avail;
		@Option(names = { "--status" }, required = true, description = "Liste tous les produits du client")
		boolean status;
		@Option(names = {
				"--subscribe" }, required = true, paramLabel = "PRODUCT_ID", description = "Souscrit à un produit")
		String subscribeProductId;
		@Option(names = {
				"--unsubscribe" }, required = true, paramLabel = "PRODUCT_ID", description = "Quitte un produit")
		String unsubscribeProductId;
	}

	@Override
	public Integer call() throws Exception {
		AsciiTable at = new AsciiTable();

		if (exclusive.isAvail()) {
			List<ClientProduitDto> produits = banqueService.obtenirProduitDisponible(clientNom);
			afficheProduits(produits, at);
		} else if (exclusive.isStatus()) {
			List<ClientProduitDto> produits = banqueService.obtenirProduitSouscrit(clientNom);
			afficheProduits(produits, at);
		} else if (StringUtils.isNotBlank(exclusive.getSubscribeProductId())) {
			ClientProduitDto produit = banqueService.abonnerProduit(clientNom, exclusive.subscribeProductId);
			if (produit != null) {
				afficheProduit(produit, at);
			}
		} else if (StringUtils.isNotBlank(exclusive.getUnsubscribeProductId())) {
			ClientProduitDto produit = banqueService.desabonnerProduit(clientNom, exclusive.unsubscribeProductId);
			if (produit != null) {
				afficheProduit(produit, at);
			}
		} else {
			// what is the answer to life the universe and everything
			return 42;
		}
		at.getRenderer().setCWC(new CWC_LongestLine());
		at.setPaddingLeftRight(1);
		at.getContext().setFrameLeftMargin(3);

		String rend = at.render();
		System.out.println(rend);

		return 0;

	}

	private void afficheEnteteProduit(AsciiTable at) {
		at.addRule();
		at.addRow(null, null, "Produit");
		at.addRule();
		at.addRow("ID", "Nom", "Statut");
		at.addRule();
	}

	private void afficheProduit(ClientProduitDto produit, AsciiTable at) {
		afficheEnteteProduit(at);
		if (produit != null && StringUtils.isNotBlank(produit.getProduitId())) {
			at.addRow(produit.getProduitId(), produit.getNom(), produit.getStatut());
			at.addRule();
		} else {
			at.addRow(null, null, "Aucun produit");
			at.addRule();
		}
	}

	private void afficheProduits(List<ClientProduitDto> produits, AsciiTable at) {
		afficheEnteteProduit(at);

		if (produits != null && !produits.isEmpty()) {
			for (ClientProduitDto p : produits) {
				at.addRow(p.getProduitId(), p.getNom(), "");
				at.addRule();
			}
		} else {
			at.addRow(null, null, "Aucun produit");
			at.addRule();
		}
	}

}
