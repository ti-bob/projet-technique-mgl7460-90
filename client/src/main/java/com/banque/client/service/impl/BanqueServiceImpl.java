package com.banque.client.service.impl;

import java.net.ConnectException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

import com.banque.client.service.BanqueService;
import com.banque.contrat.api.client.client.ClientApi;
import com.banque.contrat.api.client.client.model.ClientProduitDto;
import com.banque.contrat.api.client.client.model.ErrorResponseDto;
import com.google.gson.Gson;

@Service
public class BanqueServiceImpl implements BanqueService {

	private ClientApi clientApi;

	@Autowired
	BanqueServiceImpl(ClientApi clientApi) {
		this.clientApi = clientApi;
	}

	@Override
	public ClientProduitDto abonnerProduit(String clientNom, String subscribeProductId) {
		ClientProduitDto produitDto = null;
		try {
			produitDto = clientApi.subscribeProduits(clientNom, subscribeProductId);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return produitDto;
	}

	@Override
	public ClientProduitDto desabonnerProduit(String clientNom, String unsubscribeProductId) {
		ClientProduitDto produitDto = null;
		try {
			produitDto = clientApi.unsubscribeProduits(clientNom, unsubscribeProductId);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return produitDto;
	}

	@Override
	public List<ClientProduitDto> obtenirProduitDisponible(String clientNom) {

		List<ClientProduitDto> produitsDto = null;
		try {
			produitsDto = clientApi.getClientProduits(clientNom, false);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return produitsDto;
	}

	@Override
	public List<ClientProduitDto> obtenirProduitSouscrit(String clientNom) {
		List<ClientProduitDto> produitsDto = null;
		try {
			produitsDto = clientApi.getClientProduits(clientNom, true);
		} catch (HttpClientErrorException e) {
			Gson g = new Gson();
			ErrorResponseDto erd = g.fromJson(e.getResponseBodyAsString(), ErrorResponseDto.class);
			System.out.println(erd.getMessage());
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				System.out.println("Impossible de se connecter à la banque");
			} else {
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return produitsDto;
	}
}
