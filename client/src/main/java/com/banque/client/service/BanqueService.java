package com.banque.client.service;

import java.util.List;

import com.banque.contrat.api.client.client.model.ClientProduitDto;

public interface BanqueService {

	public ClientProduitDto abonnerProduit(String clientNom, String subscribeProductId);

	public ClientProduitDto desabonnerProduit(String clientNom, String unsubscribeProductId);

	public List<ClientProduitDto> obtenirProduitDisponible(String clientNom);

	public List<ClientProduitDto> obtenirProduitSouscrit(String clientNom);

}
