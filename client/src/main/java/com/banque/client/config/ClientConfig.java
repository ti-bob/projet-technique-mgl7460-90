package com.banque.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.banque.contrat.api.client.client.ClientApi;
import com.banque.contrat.api.client.client.handler.ApiClient;

@Configuration
public class ClientConfig {
	@Bean
	public ClientApi clientApi() {
		return new ClientApi(apiClient());
	}

	@Bean
	public ApiClient apiClient() {
		return new ApiClient();
	}
}
