package com.banque.client.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.banque.contrat.api.client.client.ClientApi;
import com.banque.contrat.api.client.client.model.ClientProduitDto;

@ExtendWith(MockitoExtension.class)
class BanqueServiceImplTest {

	@Mock
	private ClientApi clientApi;
	@InjectMocks
	private BanqueServiceImpl banqueServiceImpl;

//	@BeforeEach
//	void initUseCase() {
//		banqueServiceImpl = new BanqueServiceImpl(clientApi);
//	}

	@Test
	void obtenirProduitDisponible() {

		List<ClientProduitDto> produits = new ArrayList<ClientProduitDto>();

		Mockito.when(clientApi.getClientProduits(ArgumentMatchers.anyString(), ArgumentMatchers.anyBoolean()))
				.thenReturn(produits);

		List<ClientProduitDto> reponse = banqueServiceImpl.obtenirProduitDisponible("UnClient");

		Assertions.assertEquals(produits, reponse);
	}

	@Test
	void obtenirProduitSouscrit() {

		List<ClientProduitDto> produits = new ArrayList<ClientProduitDto>();

		Mockito.when(clientApi.getClientProduits(ArgumentMatchers.anyString(), ArgumentMatchers.anyBoolean()))
				.thenReturn(produits);

		List<ClientProduitDto> reponse = banqueServiceImpl.obtenirProduitSouscrit("UnClient");

		Assertions.assertEquals(produits, reponse);
	}

	@Test
	void desabonnerProduit() {
		ClientProduitDto produit = new ClientProduitDto();

		Mockito.when(clientApi.unsubscribeProduits(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()))
				.thenReturn(produit);

		ClientProduitDto reponse = banqueServiceImpl.desabonnerProduit("clientNom", "unsubscribeProductId");

		Assertions.assertEquals(produit, reponse);

	}

}
